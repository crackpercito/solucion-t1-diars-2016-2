# **Solución Examen T1 Diars 2016 -2 UPN Cajamarca**

## Pregunta 01

- Modificar el Archivo **RouteConfig.cs**

```csharp
routes.MapRoute(
    name: "Default",
    url: "{controller}/{action}/{id}",
    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
    namespaces:new []{ "DiarsT1.Controllers" } //Solo se tenia que agrear esta linea
);
```

## Pregunta 02
![](images/p3.jpg)

- En el **HomeController** del area Juegos:

>**Toda la solución se implemtaba usando sesiones**

```csharp
        public ActionResult Adivina()
        {
            var rand = new Random();
            var nro = rand.Next(1, 100);

            if (HttpContext.Session != null) HttpContext.Session.Add("nro", nro);

            ViewBag.Nro = nro;

            return View();
        }

        [HttpPost]
        public ActionResult Adivina(int nro)
        {
            if (HttpContext.Session != null)
            {
                var generado =  Int32.Parse(HttpContext.Session["nro"].ToString());

                if (generado.Equals(nro))
                {
                    ViewBag.Nro = generado;
                    ViewBag.Suerte = nro;
                    ViewBag.Msg = "Que Suerte divinaste";
                }
                else if (nro > generado)
                {
                    ViewBag.Nro = generado;
                    ViewBag.Suerte = nro;
                    ViewBag.Msg = "demaciado alto prueba otra vez";
                }
                else if (nro < generado)
                {
                    ViewBag.Nro = generado;
                    ViewBag.Suerte = nro;
                    ViewBag.Msg = "demaciado bajo prueba otra vez";
                }
            }

            return View();
        }
```

- En la vista correspondiente al Action Adivina **Adivina.cshtml**

```html
<h2>Adivina El Número</h2>
<h3>Nro Generado: @ViewBag.Nro</h3>

@if (ViewBag.Msg != null)
{
    <div class="alert alert-info">
        <h4>@ViewBag.Msg</h4>
    </div>
}

@using (Html.BeginForm())
{
    <div class="form-group">
        <label>Ingresa el Nro:</label>
        <input type="text" name="nro" value="@ViewBag.Suerte" class="form-control" />
    </div>

    <button class="btn btn-warning">Probar Suerte</button>
    @Html.ActionLink("Nuevo Juevo", "Adivina", null, new { @class = "btn btn-info" })
}

```

## Pregunta 03

![](images/p3.jpg)

- En el **HomeController** del area Juegos:

>**Toda la solución se implemtaba usando sesiones**

```csharp
//incializamos variables
public ActionResult Index()
{
    if (HttpContext.Session != null)
    {
        HttpContext.Session.Add("punto",0);
        HttpContext.Session.Add("sumaDados", 0);
        HttpContext.Session.Add("traking","");
    }

    return View();
}
```
- En la vista correspondiente al Action Index **Index.cshtml**

```html
@using (Html.BeginForm("Jugar","Home"))
{
    <button class="btn btn-warning">Tirar Dados</button>
    @Html.ActionLink("Nuevo Juevo","Index",null,new {@class="btn btn-info"})

    if(ViewBag.Traking != null)
    {

        <div class="panel panel-danger" style="margin-top: 20px;">
            <div class="panel-heading">
                Traking Juego
            </div>
            <div class="panel-body">
                @Html.Raw(ViewBag.Traking)
            </div>
        </div>
    }
}
```

- Implementamos el Juego

```csharp
        [HttpPost]
        public ActionResult Jugar()
        {
            TirarDados();

            if (HttpContext.Session != null)
            {
                var punto = Int32.Parse(HttpContext.Session["punto"].ToString());
                var sumaDados = Int32.Parse(HttpContext.Session["sumaDados"].ToString());
                var traking = "";

                if (punto == 0)
                {
                    switch (sumaDados)
                    {
                        case 7:
                        case 11:
                            traking = "<b>El jugador gana</b>";
                            HttpContext.Session["traking"] = HttpContext.Session["traking"] + traking;
                            break;
                        case 2:
                        case 3:
                        case 12:
                            traking = "<b>El jugador pierde</b>";
                            HttpContext.Session["traking"] = HttpContext.Session["traking"] + traking;

                            break;
                        default:
                            punto = sumaDados;
                            HttpContext.Session["punto"] = punto;

                            traking = $"<b>El punto es {punto}</b><br/>";

                            HttpContext.Session["traking"] = HttpContext.Session["traking"] + traking;

                            break;
                    }
                }
                else if (punto != 0)
                {

                    if (sumaDados == 7)
                    {
                        traking =  "<b>El jugador pierde</b>";
                        HttpContext.Session["traking"] = HttpContext.Session["traking"] + traking;
                    }
                    else if (sumaDados == punto)
                    {
                        traking = "<b>El jugador gana</b>";
                        HttpContext.Session["traking"] = HttpContext.Session["traking"] + traking;
                    }
                }
            }

            if (HttpContext.Session != null) ViewBag.Traking = HttpContext.Session["traking"];

            return View("Index");
        }

        //método para generar dados
        [NonAction]
        public void TirarDados()
        {
            var rand = new Random(DateTime.Now.Millisecond);
            var dado1 = rand.Next(1, 6);
            var dado2 = rand.Next(1, 6);

            var  sumaDados = dado1 + dado2;

            if (HttpContext.Session != null)
            {
                HttpContext.Session["sumaDados"] = sumaDados;
                var item = $"El Jugador Tiro: {dado1} + {dado2} = {sumaDados}<br/>";

                HttpContext.Session["traking"] = HttpContext.Session["traking"] + item;
            }
        }
```
