﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace DiarsT1.Areas.Juegos.Controllers
{

    public class HomeController : Controller
    {
        // GET: Juegos/Home

        //[Route("index")]
        public ActionResult Index()
        {
            if (HttpContext.Session != null)
            {
                HttpContext.Session.Add("punto",0);
                HttpContext.Session.Add("sumaDados", 0);
                HttpContext.Session.Add("traking","");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Jugar()
        {
            TirarDados();

            if (HttpContext.Session != null)
            {
                var punto = Int32.Parse(HttpContext.Session["punto"].ToString());
                var sumaDados = Int32.Parse(HttpContext.Session["sumaDados"].ToString());
                var traking = "";

                if (punto == 0)
                {
                    switch (sumaDados)
                    {
                        case 7:
                        case 11:
                            traking = "<b>El jugador gana</b>";
                            HttpContext.Session["traking"] = HttpContext.Session["traking"] + traking;
                            break;
                        case 2:
                        case 3:
                        case 12:
                            traking = "<b>El jugador pierde</b>";
                            HttpContext.Session["traking"] = HttpContext.Session["traking"] + traking;

                            break;
                        default:
                            punto = sumaDados;
                            HttpContext.Session["punto"] = punto;

                            traking = $"<b>El punto es {punto}</b><br/>";

                            HttpContext.Session["traking"] = HttpContext.Session["traking"] + traking;

                            break;
                    }
                }
                else if (punto != 0)
                {

                    if (sumaDados == 7)
                    {
                        traking =  "<b>El jugador pierde</b>";
                        HttpContext.Session["traking"] = HttpContext.Session["traking"] + traking;
                    }
                    else if (sumaDados == punto)
                    {
                        traking = "<b>El jugador gana</b>";
                        HttpContext.Session["traking"] = HttpContext.Session["traking"] + traking;
                    }
                }
            }

            if (HttpContext.Session != null) ViewBag.Traking = HttpContext.Session["traking"];

            return View("Index");
        }

        [NonAction]
        public void TirarDados()
        {
            var rand = new Random(DateTime.Now.Millisecond);
            var dado1 = rand.Next(1, 6);
            var dado2 = rand.Next(1, 6);

            var  sumaDados = dado1 + dado2;

            if (HttpContext.Session != null)
            {
                HttpContext.Session["sumaDados"] = sumaDados;
                var item = $"El Jugador Tiro: {dado1} + {dado2} = {sumaDados}<br/>";

                HttpContext.Session["traking"] = HttpContext.Session["traking"] + item;
            }
        }

        public ActionResult Adivina()
        {
            var rand = new Random();
            var nro = rand.Next(1, 100);

            if (HttpContext.Session != null) HttpContext.Session.Add("nro", nro);

            ViewBag.Nro = nro;

            return View();
        }

        [HttpPost]
        public ActionResult Adivina(int nro)
        {
            if (HttpContext.Session != null)
            {
                var generado =  Int32.Parse(HttpContext.Session["nro"].ToString());

                if (generado.Equals(nro))
                {
                    ViewBag.Nro = generado;
                    ViewBag.Suerte = nro;
                    ViewBag.Msg = "Que Suerte divinaste";
                }
                else if (nro > generado)
                {
                    ViewBag.Nro = generado;
                    ViewBag.Suerte = nro;
                    ViewBag.Msg = "demaciado alto prueba otra vez";
                }
                else if (nro < generado)
                {
                    ViewBag.Nro = generado;
                    ViewBag.Suerte = nro;
                    ViewBag.Msg = "demaciado bajo prueba otra vez";
                }
            }

            return View();
        }
    }
}