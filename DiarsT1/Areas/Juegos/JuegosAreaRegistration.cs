﻿using System.Web.Mvc;

namespace DiarsT1.Areas.Juegos
{
    public class JuegosAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Juegos";
            }
        }

       
        public override void RegisterArea(AreaRegistrationContext context) 
        {

            context.MapRoute(
                "Juegos_default",
                "Juegos/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}