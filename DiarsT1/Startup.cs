﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DiarsT1.Startup))]
namespace DiarsT1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
